import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='ffcommon',
    version=__import__('ffcommon').VERSION,
    packages=find_packages(),
    include_package_data=True,
    license='MIT',
    description='Common things for wagtail apps.',
    long_description=README,
    author='FFIT',
    author_email='support@ffit.lv',
    install_requires=[
        'Django>=1.11',
    ]
)

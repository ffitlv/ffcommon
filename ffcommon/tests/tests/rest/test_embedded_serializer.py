from django.test import TestCase

from ffcommon.ffrest.serializers import CompositeSerializer, EmbeddedSerializer
from ffcommon.tests.models import Parent, Child, Node


class SerializerMissingPk(EmbeddedSerializer):
    class Meta:
        model = Parent
        fields = ('name', )


class Embed(EmbeddedSerializer):
    class Meta:
        model = Parent
        fields = '__all__'
        read_only_fields = ('name',)


class WithEmbed(CompositeSerializer):
    parent = Embed('parent')

    class Meta:
        model = Child
        fields = '__all__'


class EmbeddedSerializerTestCase(TestCase):
    def test_missing_pk_has_to_rise(self):
        with self.assertRaises(AssertionError):
            _ = SerializerMissingPk().fields

    def test_default_pk_is_not_read_ony(self):
        s = Embed()
        id_field = s.fields['id']
        # For mapping existing fields
        self.assertFalse(id_field.read_only)
        # For creating new
        self.assertFalse(id_field.required)

    def test_create(self):
        p = Parent.objects.create(name='parent')
        s = WithEmbed()
        res = s.create({
            'name': 'child',
            'parent': {
                'id': p.id
            }
        })

        self.assertIsInstance(res, Child)
        self.assertEqual(res.parent, p)

    def test_update_set(self):
        p1 = Parent.objects.create(name='parent 1')
        # p2 = Parent.objects.create(name='parent 2')
        c = Child.objects.create(name='child')
        s = WithEmbed()
        res = s.update(c, {
            'name': 'updated child',
            'parent': {
                'id': p1.id
            }
        })

        self.assertEqual(res.parent, p1)
        self.assertEqual(res.name, 'updated child')

    def test_update_null(self):
        p1 = Parent.objects.create(name='parent 1')
        c = Child.objects.create(name='child', parent=p1)
        s = WithEmbed()
        res = s.update(c, {
            'name': 'updated child',
            'parent': None
        })

        self.assertEqual(res.parent,None)
        self.assertEqual(res.name, 'updated child')

    def test_update_change(self):
        p1 = Parent.objects.create(name='parent 1')
        p2 = Parent.objects.create(name='parent 2')
        c = Child.objects.create(name='child', parent=p1)
        s = WithEmbed()
        res = s.update(c, {
            'name': 'updated child',
            'parent': {
                'id': p2.id
            }
        })

        self.assertEqual(res.parent, p2)
        self.assertEqual(res.name, 'updated child')


class NodeSerializer(EmbeddedSerializer):
    class Meta:
        model = Node
        fields = ('id', )


class ParentSerializer(CompositeSerializer):
    nodes = NodeSerializer(many=True)

    class Meta:
        model = Parent
        fields = ('name', 'nodes',)
        read_only_fields = ('name',)


class EmbeddedListSerializerTestCase(TestCase):
    def test_create(self):
        n1 = Node.objects.create()
        n2 = Node.objects.create()
        s = ParentSerializer()
        res = s.create({
            'name': 'ParentName',
            'nodes': [
                {
                    'id': n1.pk
                }, {
                    'id': n2.pk
                }
            ]
        })

        self.assertIsInstance(res, Parent)
        self.assertEqual(res.name, "ParentName")
        self.assertEqual(res.nodes.all().count(), 2)

    def test_update(self):
        n1 = Node.objects.create()
        n2 = Node.objects.create()
        p = Parent.objects.create(name='parent')
        p.nodes.set([n1])

        s = ParentSerializer()
        res = s.update(p, {
            'nodes': [
                {
                    'id': n1.pk
                }, {
                    'id': n2.pk
                }
            ]
        })

        self.assertEqual(p.nodes.all().count(), 2)

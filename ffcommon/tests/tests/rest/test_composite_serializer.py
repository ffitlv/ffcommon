from django.test import TestCase

from ffcommon.ffrest.serializers import CompositeSerializer, NestedSerializer
from ffcommon.tests.models import Parent, Child, SubChild, SubSubChild


class SubSubChildSerializer(NestedSerializer):
    class Meta:
        model = SubSubChild
        fields = '__all__'


class SubChildSerializer(NestedSerializer):
    child = SubSubChildSerializer()

    class Meta:
        model = SubChild
        fields = '__all__'


class ChildSerializer(NestedSerializer):
    children = SubChildSerializer(many=True)

    class Meta:
        model = Child
        fields = '__all__'


class ParentSerializer(CompositeSerializer):
    children = ChildSerializer(many=True, required=False)

    class Meta:
        model = Parent
        fields = '__all__'


class NestedSerializerTestCase(TestCase):
    def test_prepare_immediate_list(self):
        parent = Parent.objects.create(name='parent')
        s = ChildSerializer()
        is_factory, i = s.prepare_field({
            'name': 'child',
            'children': [
                {'name': 'a'},
                {'name': 'b'}
            ]
        }, parent, forward_field='parent')

        self.assertFalse(is_factory)

        self.assertIsInstance(i, Child)
        self.assertEqual(i.name, 'child')
        self.assertEqual(i.parent, parent)

        # children create
        SubChild.objects.get(parent=i, name='a')
        SubChild.objects.get(parent=i, name='b')

    def test_prepare_delayed_list(self):
        s = ChildSerializer()
        is_factory, result = s.prepare_field({
            'name': 'child',
            'children': [
                {'name': 'a'},
                {'name': 'b'}
            ]
        }, forward_field='parent')

        self.assertTrue(is_factory)
        self.assertTrue(hasattr(result, '__call__'))

        parent = Parent.objects.create(name='parent')
        i = result(parent)

        self.assertIsInstance(i, Child)
        self.assertEqual(i.name, 'child')
        self.assertEqual(i.parent, parent)

        # children create
        SubChild.objects.get(parent=i, name='a')
        SubChild.objects.get(parent=i, name='b')

    def test_prepare_delayed(self):
        s = SubChildSerializer()
        is_factory, result = s.prepare_field({'name': 'child'}, forward_field='parent')

        self.assertTrue(is_factory)
        self.assertTrue(hasattr(result, '__call__'))

        parent = Child.objects.create(name='parent')
        i = result(parent)

        self.assertIsInstance(i, SubChild)
        self.assertEqual(i.name, 'child')
        self.assertEqual(i.parent, parent)

    def test_prepare_immediate(self):
        parent = Child.objects.create(name='parent')
        s = SubChildSerializer()
        is_facotry, result = s.prepare_field({'name': 'child'}, parent, forward_field='parent')
        self.assertFalse(is_facotry)
        self.assertIsInstance(result, SubChild)
        self.assertEqual(result.name, 'child')
        self.assertEqual(result.parent, parent)

    def test_prepare_update(self):
        parent = Child.objects.create(name='parent')
        i = SubChild.objects.create(name='sub')
        s = SubChildSerializer('parent')
        is_factory, result = s.prepare_field({'id': i.id, 'name': 'sub updated'}, parent)
        result.save()
        SubChild.objects.get(pk=i.id, name='sub updated')

    def test_recursive(self):
        s = SubChildSerializer('parent')
        is_factory, result = s.prepare_field({
            'name': 'parent',
            'child': {
                'name': 'child'
            }
        })

        self.assertTrue(is_factory)
        self.assertTrue(hasattr(result, '__call__'))

        i = result(None)
        self.assertIsInstance(i, SubChild)
        self.assertEqual(i.name, 'parent')
        self.assertIsInstance(i.child, SubSubChild)

    def test_create(self):
        s = ParentSerializer()
        i = s.create({
            'name': 'root',
            'children': [{
                'name': 'A',
                'children': [{
                    'name': 'A1'
                }, {
                    'name': 'A2'
                }]
            }, {
                'name': 'child B',
                'children': [{
                    'name': 'B1'
                }, {
                    'name': 'B2'
                }]
            }]
        })

    def test_update(self):
        s = ParentSerializer()
        parent = Parent.objects.create(name='parent')
        child = Child.objects.create(name='A', parent=parent)
        subchild = SubChild.objects.create(name='A1', parent=child)

        DATA = {
            'name': 'parent updated',
            'children': [{
                'id': child.id,
                'name': 'A updated',
                'children': [{
                    'id': subchild.id,
                    'name': 'A1 updated',
                    'child': {
                        'name': 'A11'
                    }
                }, {
                    'name': 'A2'
                }]
            }, {
                'name': 'child B',
                'children': [{
                    'name': 'B1'
                }, {
                    'name': 'B2'
                }]
            }]
        }
        res = s.update(parent, DATA)
        self.assertEqual(res.name, 'parent updated')
        self.assertEqual(res.children.count(), 2)
        a = Child.objects.get(parent=res, name='A updated', id=child.id)
        self.assertEqual(a.children.count(), 2)
        a1 = SubChild.objects.get(parent=a, name='A1 updated', id=subchild.id)
        self.assertEqual(a1.child.name, 'A11')
        a2 = SubChild.objects.get(parent=a, name='A2')

        b = Child.objects.get(parent=res, name='child B')
        self.assertEqual(b.children.count(), 2)
        b1 = SubChild.objects.get(parent=b, name='B1')
        b2 = SubChild.objects.get(parent=b, name='B2')

        # we remove A2, B2, Change A11
        res = s.update(parent, {
            'name': 'parent updated 2',
            'children': [{
                'id': child.id,
                'name': 'A updated 2',
                'children': [{
                    'id': subchild.id,
                    'name': 'A1 updated 2',
                    'child': {
                        'id': subchild.child.id,
                        'name': 'A11 updated'
                    }
                }]
            }, {
                'id': b.id,
                'name': 'child B',
                'children': [{
                    'id': b1.id,
                    'name': 'B1'
                }]
            }]
        })

        self.assertEqual(res.name, 'parent updated 2')
        self.assertEqual(res.children.count(), 2)

        a = Child.objects.get(parent=res, name='A updated 2', id=child.id)
        self.assertEqual(a.children.count(), 1)
        a1 = SubChild.objects.get(parent=a, name='A1 updated 2', id=subchild.id)
        self.assertEqual(a1.child.name, 'A11 updated')

        b = Child.objects.get(parent=res, name='child B')
        self.assertEqual(b.children.count(), 1)
        b1 = SubChild.objects.get(parent=b, name='B1', id=b1.id)


from django.test import TestCase

from ffcommon.ffrest.serializers import NestedSerializer
from ffcommon.tests.models import Parent


class Serializer(NestedSerializer):
    class Meta:
        model = Parent
        fields = ('id', 'name', )


class SerializerMissingPk(NestedSerializer):
    class Meta:
        model = Parent
        fields = ('name', )


class NestedSerializerTestCase(TestCase):
    def test_missing_pk_has_to_rise(self):
        with self.assertRaises(AssertionError):
            _ = SerializerMissingPk('dummy').fields

    def test_default_pk_is_not_read_ony(self):
        s = Serializer('dummy')
        id_field = s.fields['id']
        # For mapping existing fields
        self.assertFalse(id_field.read_only)
        # For creating new
        self.assertFalse(id_field.required)

DEBUG = True
TEMPLATE_DEBUG = DEBUG
DEBUG_PROPAGATE_EXCEPTIONS = True

SECRET_KEY = 'DERP'

INSTALLED_APPS = (
    'ffcommon.tests',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'ffcommon_test'
    }
}


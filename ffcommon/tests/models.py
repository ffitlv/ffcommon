from django.db import models


class Node(models.Model):
    pass


class Parent(models.Model):
    name = models.CharField(max_length=10)
    nodes = models.ManyToManyField(Node)


class Child(models.Model):
    name = models.CharField(max_length=10)
    parent = models.ForeignKey(Parent, related_name='children', null=True)


class SubChild(models.Model):
    name = models.CharField(max_length=10)
    parent = models.ForeignKey(Child, related_name='children', null=True)


class SubSubChild(models.Model):
    name = models.CharField(max_length=10)
    parent = models.OneToOneField(SubChild, related_name='child')
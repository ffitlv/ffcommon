from rest_framework import serializers


class EnumField(serializers.Field):
    def __init__(self, enum, *args, **kwargs):
        self.enum = enum
        super().__init__(*args, **kwargs)

    def to_representation(self, value):
        return str(value.value)

    def to_internal_value(self, data):
        return self.enum(data)

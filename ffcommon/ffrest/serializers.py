from typing import Tuple, Any, Optional, Dict, Iterable, Callable, List, Union

from django.db import transaction
from django.db.models import Model
from rest_framework import serializers
from rest_framework.serializers import ListSerializer
from rest_framework.utils import model_meta


# We want to distinguish empty from None
class Empty:
    pass


Parent = Union[Optional[Model], Empty]
FieldResult = Tuple[bool, Union[Any, Callable]]


class CompositeSerializer(serializers.ModelSerializer):
    def prepare_fields(self, field_data: Dict, parent_instance: Parent = Empty) -> Tuple[Dict, Dict]:
        data = {}
        factories = {}

        info = model_meta.get_field_info(self.Meta.model)
        forward_field = None

        for attr, value in field_data.items():
            field = self.fields.get(attr, None)
            relation = info.relations.get(attr, None)

            if relation and relation.reverse:
                forward_field = self.Meta.model._meta.get_field(attr).remote_field.name

            # @TODO something more robust, so we can get rid of generic List
            if field and (isinstance(field, CompositeSerializer) or isinstance(field, CompositeListSerializer)):
                is_factory, result = field.prepare_field(value, parent_instance, forward_field)
                if is_factory:
                    factories[attr] = result
                else:
                    data[attr] = result
            # m2m
            elif relation and relation.to_many:
                raise NotImplementedError
                # @TODO What do we do here?
                # bindable[attr] = value
            else:
                # scalar
                data[attr] = value

        return data, factories

    def bind_to_instance(self, instance, data):
        for attr, value in data.items():
            setattr(instance, attr, value)

    def _get_instance(self,
                     field_data: Dict,
                     parent_instance: Parent = Empty,
                     forward_field: Optional[str] = None
                     ):
        Klass = self.Meta.model
        pk_attr = Klass._meta.pk.name

        if pk_attr in field_data:
            pk = field_data.pop(pk_attr, None)
            if pk:
                return Klass.objects.get(pk=pk)
            else:
                return None
        else:
            return Empty

    def prepare_field(self,
                      field_data: Dict,
                      parent_instance: Parent = Empty,
                      forward_field: Optional[str] = None
                      ) -> FieldResult:
        instance = self._get_instance(field_data, parent_instance, forward_field)
        data, factories = self.prepare_fields(field_data, instance)
        Klass = self.Meta.model

        def factory(supplied_parent):
            nonlocal instance, data
            if instance and instance is not Empty:
                self.bind_to_instance(instance, data)
                instance.save()

            else:
                instance = Klass.objects.create(**data)

            for sub_factory in factories.values():
                sub_factory(instance)

            return instance

        if parent_instance is not Empty:
            return False, factory(parent_instance)

        return True, factory

    @transaction.atomic()
    def create(self, validated_data) -> Model:
        is_factory, result = self.prepare_field(validated_data, None)
        assert not is_factory
        return result

    @transaction.atomic()
    def update(self, instance, validated_data) -> Model:
        data, factories = self.prepare_fields(validated_data, instance)

        self.bind_to_instance(instance, data)
        instance.save()

        for sub_factory in factories.values():
            sub_factory(instance)

        return instance


class CompositeListSerializer(ListSerializer):
    pass


class EmbeddedListSerializer(CompositeListSerializer):
    def prepare_field(self,
                      field_data: List,
                      parent_instance: Parent = Empty,
                      forward_field: Optional[str] = None
                      ) -> FieldResult:
        def factory(supplied_instance):
            instances = [self.child.Meta.model.objects.get(**item_data) for item_data in field_data]
            # m2m
            getattr(supplied_instance, self.field_name).set(instances)
            pass
        return True, factory

class EmbeddedSerializer(CompositeSerializer):
    @property
    def pk_name(self) -> str:
        return self.Meta.model._meta.pk.name

    def get_field_names(self, declared_fields, info):
        fields = super().get_field_names(declared_fields, info)
        # pk has to be present
        assert info.pk.name in fields
        return fields

    def get_extra_kwargs(self):
        kwargs = super().get_extra_kwargs()
        if self.pk_name not in kwargs:
            kwargs[self.pk_name] = {
                'read_only': False,
                'required': False
            }

        # @TODO validated else?
        return kwargs

    @classmethod
    def many_init(cls, *args, **kwargs):
        kwargs['child'] = cls()
        return EmbeddedListSerializer(*args, **kwargs)

    def prepare_field(self,
                      field_data: Optional[Dict] = None,
                      parent_instance: Parent = Empty,
                      forward_field: Optional[str] = None
                      ) -> FieldResult:
        # nullable
        if field_data is None:
            return False, None

        return False, self.Meta.model.objects.get(**field_data)


class NestedListSerializer(CompositeListSerializer):
    def prepare_field(self,
                      field_data: List,
                      parent_instance: Parent = Empty,
                      forward_field: Optional[str] = None
                      ) -> FieldResult:
        assert forward_field
        def factory(supplied_parent):
            res = []
            nested_pks = set()
            for item_data in field_data:
                is_factory, child_inst = self.child.prepare_field(item_data, supplied_parent, forward_field)
                assert not is_factory
                res.append(child_inst)
                nested_pks.add(child_inst.pk)

            self.child.Meta.model.objects.filter(**{forward_field: supplied_parent})\
                .exclude(pk__in=nested_pks)\
                .delete()

            return res

        return True, factory


class NestedSerializer(CompositeSerializer):
    @property
    def pk_name(self) -> str:
        return self.Meta.model._meta.pk.name

    def get_field_names(self, declared_fields, info):
        fields = super().get_field_names(declared_fields, info)
        # pk has to be present
        assert info.pk.name in fields
        return fields

    def get_extra_kwargs(self):
        kwargs = super().get_extra_kwargs()
        if self.pk_name not in kwargs:
            kwargs[self.pk_name] = {
                'read_only': False,
                'required': False
            }

        # @TODO validated else?

        return kwargs

    @classmethod
    def many_init(cls, *args, **kwargs):
        kwargs['child'] = cls()
        return NestedListSerializer(*args, **kwargs)

    def prepare_field(self,
                      field_data: Dict,
                      parent_instance: Parent = Empty,
                      forward_field: Optional[str] = None
                      ) -> FieldResult:


        instance = self._get_instance(field_data, parent_instance, forward_field)

        data, factories = self.prepare_fields(field_data, instance)
        Klass = self.Meta.model

        def factory(supplied_parent):
            nonlocal instance, data

            # When it is None?
            if forward_field:
                data[forward_field] = supplied_parent

            if instance and instance is not Empty:
                self.bind_to_instance(instance, data)
                instance.save()
            else:
                instance = Klass.objects.create(**data)

            for sub_factory in factories.values():
                sub_factory(instance)

            return instance

        if parent_instance is not Empty:
            return False, factory(parent_instance)

        return True, factory


class NestedOneToOneSerializer(CompositeSerializer):

    # @TODO assert field type
    # @TODO exclude pk

    def prepare_field(self,
                      field_data: Dict,
                      parent_instance: Parent = Empty,
                      forward_field: Optional[str] = None
                      ) -> FieldResult:
        assert forward_field
        Klass = self.Meta.model

        if parent_instance and parent_instance is not Empty:
            try:
                instance = Klass.objects.get(**{forward_field: parent_instance})
            except Klass.DoesNotExist:
                instance = None
        else:
            instance = Empty

        if field_data is None:
            return False, None

        data, factories = self.prepare_fields(field_data, instance)

        def factory(supplied_parent):
            nonlocal instance, data
            data[forward_field] = supplied_parent

            if instance and instance is not Empty:
                self.bind_to_instance(instance, data)
                instance.save()
            else:
                instance = Klass.objects.create(**data)

            for sub_factory in factories.values():
                sub_factory(instance)

            return instance

        if parent_instance is not Empty:
            return False, factory(parent_instance)

        return True, factory
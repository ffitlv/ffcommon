from ffcommon.ffcore.utils import TranslatablePanels
from django.db import models
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel
from wagtail.wagtailcore.models import Orderable
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailimages.models import Image


class Gallery(ClusterableModel):
    title = models.CharField(max_length=255)
    panels = TranslatablePanels(FieldPanel('title'), InlinePanel('images'))

    class Meta:
        abstract = True


class GalleryImage(Orderable):
    gallery = ParentalKey(Gallery, related_name="images")
    image = models.ForeignKey(Image)
    panels = [
        ImageChooserPanel('image')
    ]

    class Meta:
        abstract = True

from django.forms import Select, ModelChoiceField
from wagtail.wagtailcore.blocks import ChooserBlock
from django.utils.functional import cached_property
from wagtail.wagtailimages.shortcuts import get_rendition_or_not_found


class GalleryBlock(ChooserBlock):
    widget = Select

    # @TODO pass down slick props
    def __init__(self, queryset, image_spec='original', show_thumbs=False, **kwargs):
        self.image_spec = image_spec
        self.queryset = queryset
        self.target_model = queryset.model
        self.show_thumbs = show_thumbs
        super().__init__(**kwargs)

    def get_context(self, value, parent_context=None):
        ctx = super().get_context(value, parent_context)
        wagtail_images = [v.image for v in value.images.all()]

        ctx.update({
            'images': [get_rendition_or_not_found(v, self.image_spec).img_tag() for v in wagtail_images],
            'id': "{}-{}".format(self.definition_prefix, self.name)
        })
        if self.show_thumbs:
            # @TODO thumb spec
            ctx['thumbs'] = [get_rendition_or_not_found(v, self.image_spec).img_tag(extra_attributes={'class': 'thumb'})
                             for v in wagtail_images]

        ctx['image_spec'] = self.image_spec
        return ctx

    def value_from_form(self, value):
        if value != '':
            return super().value_from_form(value)

    def value_for_form(self, value):
        if isinstance(value, self.target_model):
            return value.pk
        else:
            return value

    @cached_property
    def field(self):
        return ModelChoiceField(queryset=self.queryset, required=True)

    class Meta:
        template = 'ffgallery/block/slick_gallery.html'
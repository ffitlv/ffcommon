from django.apps import AppConfig
from django.utils.module_loading import autodiscover_modules
from ffcommon.ffmenu.menu import manager


class FfmenuConfig(AppConfig):
    name = 'ffcommon.ffmenu'

    def ready(self):
        autodiscover_modules('menu', register_to=manager)

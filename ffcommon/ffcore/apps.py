from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'ffcommon.ffcore'

    def ready(self):
        def ready(self):
            import core.wagtail_hooks

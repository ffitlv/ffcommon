from django import template

register = template.Library()


@register.filter
def chunk(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

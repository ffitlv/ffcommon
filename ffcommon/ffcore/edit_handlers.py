from wagtail.wagtailadmin.edit_handlers import BaseFieldPanel


class ChoiceQuerysetBase(BaseFieldPanel):
    def __init__(self, instance=None, form=None):
        form[self.field_name].field.queryset = self.resolve_queryset.__func__(instance)
        super().__init__(instance, form)


class QuerysetChoiceFieldPanel(object):
    def __init__(self, field_name, resolve_queryset, classname=""):
        self.field_name = field_name
        self.classname = classname
        self.resolve_queryset = resolve_queryset

    def bind_to_model(self, model):
        base = {
            'model': model,
            'field_name': self.field_name,
            'classname': self.classname,
            'resolve_queryset': classmethod(self.resolve_queryset)
        }
        return type(str('_QuerysetChoiceFieldPanell'), (ChoiceQuerysetBase,), base)

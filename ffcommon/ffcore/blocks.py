# from typing import Any, Optional, Iterable
from django.utils.html import mark_safe
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _

from django.core.exceptions import NON_FIELD_ERRORS
from wagtail.wagtailcore.blocks import StreamBlockValidationError, StreamBlock
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailimages.shortcuts import get_rendition_or_not_found


class ImageBlock(ImageChooserBlock):
    INLINE = 'inline'
    BACKGROUND = 'background'

    def __init__(self, renderer=INLINE, classname='img-fluid', image_spec='original', wrap=False, *args, **kwargs):
        self.renderer = renderer
        self.classname = classname
        self.image_spec = image_spec
        self.wrap = wrap
        super().__init__(*args, **kwargs)

    def render(self, value, context=None):
        # @TODO titles
        rendition = get_rendition_or_not_found(value, self.image_spec)
        if self.renderer == ImageBlock.INLINE:
            res = rendition.img_tag(extra_attributes={'class': self.classname})

        elif self.renderer == ImageBlock.BACKGROUND:
            res = '<div class="{}" style="background-image: url(\'{}\');"></div>'.format(self.classname, rendition.url)

        else:
            raise ValueError("Unknown renderer.")

        return mark_safe('<div class="image-wrap">{}</div>'.format(res) if self.wrap else res)


class RowBlock(StreamBlock):
    def __init__(self,
                 local_blocks=None,
                 classname='row',
                 columns=None,
                 **kwargs):
        self.classname = classname
        self.columns = columns
        super().__init__(local_blocks, **kwargs)

    @property
    def column_classes(self):
        return self.columns or self.meta.columns

    def get_context(self, value, parent_context=None):
        ctx = super().get_context(value, parent_context)

        if self.column_classes is not None:
            columns = zip(self.column_classes, value)
        else:
            columns = [('col', c) for c in value]

        ctx.update({
            'row_class': self.classname or self.meta.classname,
            'columns': columns
        })
        return ctx

    def clean(self, value):
        if self.column_classes is not None:
            required_columns = len(list(self.column_classes))
            if len(value) != required_columns:
                raise StreamBlockValidationError(non_block_errors=[
                    _("This row has to contain %(columns)s columns") % {'columns': required_columns}
                ])

        return super().clean(value)

    class Meta:
        template = 'ffcommon/block/row.html'
        classname = None
        columns = None

    # This is here just to overload admin template :(
    def render_list_member(self, block_type_name, value, prefix, index, errors=None):
        """
        Render the HTML for a single list item. This consists of an <li> wrapper, hidden fields
        to manage ID/deleted state/type, delete/reorder buttons, and the child block's own HTML.
        """
        child_block = self.child_blocks[block_type_name]
        child = child_block.bind(value, prefix="%s-value" % prefix, errors=errors)
        return render_to_string('ffcommon/block_forms/col.html', {
            'child_blocks': self.child_blocks.values(),
            'block_type_name': block_type_name,
            'prefix': prefix,
            'child': child,
            'index': index,
        })

    # This is here just to overload admin template :(
    def render_form(self, value, prefix='', errors=None):
        error_dict = {}
        if errors:
            if len(errors) > 1:
                # We rely on StreamBlock.clean throwing a single
                # StreamBlockValidationError with a specially crafted 'params'
                # attribute that we can pull apart and distribute to the child
                # blocks
                raise TypeError('StreamBlock.render_form unexpectedly received multiple errors')
            error_dict = errors.as_data()[0].params

        # value can be None when the StreamField is in a formset
        if value is None:
            value = self.get_default()
        # drop any child values that are an unrecognised block type
        valid_children = [child for child in value if child.block_type in self.child_blocks]

        list_members_html = [
            self.render_list_member(child.block_type, child.value, "%s-%d" % (prefix, i), i,
                                    errors=error_dict.get(i))
            for (i, child) in enumerate(valid_children)
        ]

        return render_to_string('ffcommon/block_forms/row.html', {
            'prefix': prefix,
            'list_members_html': list_members_html,
            'child_blocks': sorted(self.child_blocks.values(), key=lambda child_block: child_block.meta.group),
            'header_menu_prefix': '%s-before' % prefix,
            'block_errors': error_dict.get(NON_FIELD_ERRORS),
        })


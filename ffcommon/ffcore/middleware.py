from django.conf import settings
from django.utils.translation import activate, deactivate


class LanguageSegmentMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        self.langauge_codes = list([code for code, _name in settings.LANGUAGES])

    def __call__(self, request):
        language = request.get_full_path().split('/')[1]
        force = language in self.langauge_codes

        if force:
            activate(language)

        response = self.get_response(request)

        if force:
            deactivate()

        return response


class AdminLanguageOverrideMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        force = request.path.startswith('/admin') # @TODO make this a setting

        if force:
            activate('en') # @TODO make this a setting

        response = self.get_response(request)

        if force:
            deactivate()

        return response


from django.apps import apps
from django.conf import settings
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel


LANGUAGE_CODES = (code for code, name in settings.LANGUAGES)


class TranslatablePanels(object):
    def __init__(self, *panels):
        self._panels = panels
        self.panels = None

    def __get__(self, obj, model):
        if not self.panels:
            self.panels = self.setup_translations(model)

        return self.panels

    def generate_translated_panels(self, translatable):
        for panel in self._panels:
            # @TODO other field types
            if isinstance(panel, FieldPanel) and panel.field_name in translatable:
                yield MultiFieldPanel(
                    [FieldPanel(
                        "{}_{}".format(panel.field_name, c),
                        classname=panel.classname,
                        widget=panel.widget
                    ) for c in LANGUAGE_CODES
                ], heading=panel.field_name)  # @TODO translate?
            else:
                yield panel

    def setup_translations(self, model):
        if apps.is_installed('modeltranslation'):
            from modeltranslation.translator import translator
            if model in translator._registry:
                fields = translator.get_options_for_model(model).get_field_names()
                return list(self.generate_translated_panels(fields))

        return self._panels
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.forms import Select
from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.utils import translation
from ffcommon.ffcore.edit_handlers import QuerysetChoiceFieldPanel
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailcore.models import Page


OMMIT_PREFIX = getattr(settings, 'OMMIT_LANGUAGE_PREFIX', False)


class TranslatableSiteRoot(Page):
    def translation_roots(self):
        return TranslationRoot.objects.filter(
            depth=self.depth + 1,
            path__range=self._get_children_path_interval(self.path)
        )

    def serve(self, request, *args, **kwargs):
        language = translation.get_language()

        try:
            root = self.translation_roots().get(language=language)
            if language == settings.LANGUAGE_CODE and OMMIT_PREFIX:
                return root.serve(request, *args, **kwargs)

            return HttpResponseRedirect(self.url + language + '/')

        except TranslationRoot.DoesNotExist:
            return HttpResponseNotFound("Translation not found")

    @classmethod
    def can_create_at(cls, parent):
        return super().can_create_at(parent) and not cls.objects.exists()


class TranslationRoot(Page):
    language = models.CharField(max_length=2, unique=True)
    home_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='home_of'
    )

    content_panels = [
        FieldPanel('language', widget=Select(choices=settings.LANGUAGES)),
        QuerysetChoiceFieldPanel('home_page',
                                 lambda instance: instance.get_children() if instance else Page.objects.none())
    ]

    settings_panels = []
    promote_panels = []
    parent_page_types = [TranslatableSiteRoot]

    @classmethod
    def can_exist_under(cls, parent):
        # Can't nest translation roots
        if isinstance(parent, TranslationRoot):
            return False

        return super().can_exist_under(parent)

    def serve(self, request, *args, **kwargs):
        if self.home_page:
            return self.home_page.specific.serve(request, *args, **kwargs)

        first_child = self.get_children().first()

        if first_child:
            return first_child.specific.serve(request, *args, **kwargs)
        else:
            return HttpResponseNotFound("This translation has no home page")

    def save(self, *args, **kwargs):
        self.title = "Translation: {}".format(self.get_language_display())
        self.slug = self.language
        super().save(*args, **kwargs)


class PageLink(models.Model):
    pass


class TranslatablePage(Page):
    link = models.ForeignKey(PageLink, related_name="%(app_label)s_%(class)s_linked_pages",
                             null=True, blank=True, on_delete=models.SET_NULL)

    @classmethod
    def can_create_at(cls, parent):
        return super().can_create_at(parent) and (
            isinstance(parent, TranslationRoot) or isinstance(parent, TranslatablePage)
        )

    settings_panels = Page.settings_panels + [FieldPanel('link')]

    @property
    def translation_root(self):
        return self.get_ancestors()\
            .get(content_type=ContentType.objects.get_for_model(TranslationRoot)).specific

    @property
    def language(self):
        return self.translation_root.language

    def can_move_to(self, parent):
        # if is used as home can't be moved elsewhere
        if TranslationRoot.objects.filter(home_page=self).exclude(pk=parent.pk).exists():
            return False

        return super().can_move_to(parent)

    class Meta:
        abstract = True


    # @TODO linked pages